# Add To Event - SRE Tech Test

The AddToEvent Site Reliability Engineer Tech Test.

This is a live coding tech test. 

This test is expected to be ran by an Interviewer at AddToEvent Ltd. 

It is expected that the Interviewee shares their screen and explains what steps they are performing.

The Interviewer may ask questions prior, during, and after this test.

## Brief 

1. Take 5 minutes to read through the "How to deploy Service A" runbook
2. Deploy Service A to the 'sre' Google Kubernetes Engine cluster in the `ate-sre-test` Google Cloud Platform project
3. Check that Service A is running correctly
4. Answers questions from the interviewer 

## Help 

If you get stuck or do not know something please ask, this should be a low-pressure test. 
