# How to deploy Service A

This runbook goes over how to deploy Service A using the Helm CLI.

| Owner        | Support Contacts |
|:-------------|:-----------------|
| @tom.chinery | @tom.chinery     |

## Background

At AddToEvent we generally use Helm to manage our Kubernetes applications. We store our container images 
in Google Container Registry. 

Service A is an image (gcr.io/ate-sre-test/service-a) with Health and Liveness probe checks that has been 
built and pushed to GCR by our Engineering team. 

This runbook covers how to deploy Service A to the Google Kubernetes Cluster in the Google Cloud Platform project `ate-sre-test`.

## Required Tools

> - [Cloud SDK](https://cloud.google.com/sdk/docs/install)
> - [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
> - [Helm](https://helm.sh/docs/intro/install/)

## Required Information

The following information is required to execute this runbook:

| Information            | Description                                                                                             | Substitution                  |
|:-----------------------|---------------------------------------------------------------------------------------------------------|-------------------------------|
| GCR Image to deploy    | The GCR Image to deploy into your environments cluster                                                  | gcr.io/ate-sre-test/service-a |

## Process

1. [Authenticate Gcloud SDK with the Service Account file](#1-authenticate-gcloud-sdk-with-the-service-account-file)
2. [Set Cluster Credentials via Cloud SDK CLI](#1-set-cluster-credentials-via-cloud-sdk-cli)
3. [Helm install the latest version of Service A](#3-helm-install-the-latest-version-of-service-a)
4. [OPTIONAL: PLEASE ASK INTERVIEWER: Cleanup: Uninstall the Service A service](#4-optional-please-ask-interviewer-cleanup-uninstall-the-service-a-service)

## 1. Authenticate Gcloud SDK with the Service Account file

In the project root of this repsository there is a Service Account file; `service-account.json`. 

In order to run the following commands you will need to authenticate GCloud SDK with this service account:
```
$ gcloud auth activate-service-account deployment-sa@ate-sre-test.iam.gserviceaccount.com --key-file=./service-account.json --project=ate-sre-test
```

## 2. Set Cluster Credentials via Cloud SDK CLI

Run the following Cloud SDK command - this command will add the Cluster credentials to your `.kubeconfig` file:
```
$ gcloud container clusters get-credentials --zone "europe-west2-a" "sre" --project=ate-sre-test
```

## 3. Helm install the latest version of Service A

To install the latest version of a Service A to your environment with Helm first 
navigate to the Project Root of this repository. 

Now run the following Helm install command:
```
$ helm install service-a ./charts/developer/ --set image=gcr.io/ate-sre-test/service-a:latest
```

__Explanation of flags__:

*Release Name:*
The first argument to Helm is the name of this release - we have used `service-a`.

*Chart:*
In this example we pointed to a local chart (the one in the Project Root of the repository). You could
also use a chart from a Helm repository. 
> The install argument must be a chart reference, a path to a packaged chart, a path to an unpacked chart directory or a URL.

*Image:*
Our Charts take a single argument of `image`. This is the URL to the image you want to install in your cluster.

## 4. OPTIONAL: PLEASE ASK INTERVIEWER: Cleanup: Uninstall the Service A service

Run helm uninstall to uninstall the Service A instance
```
$ helm uninstall service-a
```
